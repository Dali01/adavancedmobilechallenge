package eu.tutorials.mobilechallenge

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import eu.tutorials.mobilechallenge.adapters.ImageAdapter
import eu.tutorials.mobilechallenge.adapters.StoryAdapter
import eu.tutorials.mobilechallenge.databinding.FragmentHomeBinding
import eu.tutorials.mobilechallenge.models.Image
import eu.tutorials.mobilechallenge.viewModel.HomeViewModel


class Home : Fragment() {

    private lateinit var storiesAdapter : StoryAdapter
    private lateinit var binding: FragmentHomeBinding
    private val homeViewModel: HomeViewModel by viewModels()
    private lateinit var imagesAdapter : ImageAdapter




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater,container,false)
        Log.e("onCreateView","Called")
        return binding.root
    }


    @SuppressLint("UseRequireInsteadOfGet")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        homeViewModel.photoList.observe(viewLifecycleOwner,  { it ->
            if(!it.isNullOrEmpty()){
                val picturesList : List<Image> = Gson()
                    .fromJson(Gson().toJson(it),Array<Image>::class.java)
                    .toList()
                Log.i("List","$picturesList")

                //Setup UI For Principal Pictures (home)
                binding.rvPictures.layoutManager = GridLayoutManager(this.context,2)
                binding.rvPictures.setHasFixedSize(true)
                imagesAdapter = this.context?.let { ImageAdapter(it,picturesList.toCollection(ArrayList<Image>())) }!!
                binding.rvPictures.adapter = imagesAdapter

                imagesAdapter.setOnClickListener(object : ImageAdapter.OnClickListener {
                    override fun onClick(position: Int, model: Image) {
                        val action = HomeDirections.actionHome2ToImageDetailsFragment(model)
                        Navigation.findNavController(view)
                            .navigate(action)
                    }

                }
                )



                //Setup UI For Stories Pictures
                binding.rvStories.layoutManager = LinearLayoutManager(this.context
                    , LinearLayoutManager.HORIZONTAL,false)

                storiesAdapter = StoryAdapter(this.requireContext(), it as ArrayList<Image>)

                binding.rvStories.adapter = storiesAdapter

                storiesAdapter.setOnClickListener(object : StoryAdapter.OnClickListener{
                    @SuppressLint("NotifyDataSetChanged")
                    override fun onClick(position: Int, model: Image) {
                        StoryPopup(requireContext(),model).show()
                    }

                }
                )

            }
        })

    }








}