package eu.tutorials.mobilechallenge

import android.app.Dialog
import android.content.Context
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.Window
import com.bumptech.glide.Glide
import eu.tutorials.mobilechallenge.databinding.StoryPopupBinding
import eu.tutorials.mobilechallenge.models.Image

class StoryPopup(
    private val superContext: Context,
    private val homeImage: Image
) : Dialog(superContext) {
    private lateinit var binding: StoryPopupBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        binding = StoryPopupBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setupComponents()
    }

    private fun setupComponents() {
        Glide
            .with(context)
            .load(Uri.parse(homeImage.urls.full))
            .centerInside()
            .into(binding.storyPopUp)

        Log.i("image","${homeImage.urls.full}")

    }
}