package eu.tutorials.mobilechallenge.adapters

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import eu.tutorials.mobilechallenge.databinding.ImageItemBinding
import eu.tutorials.mobilechallenge.models.Image

open class ImageAdapter(
    private val context: Context,
    private var list: ArrayList<Image>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var onClickListener : OnClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return MyViewHolder(
            ImageItemBinding.inflate(
                LayoutInflater.from(context),parent,
                false)
        )
    }

    fun setOnClickListener(onClickListener: OnClickListener){
        this.onClickListener = onClickListener
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = list[position]

        if (holder is MyViewHolder) {

            holder.binding.title.text = model.user.username


            Glide.with(context)
                .load(model.urls.regular)
                .placeholder(ColorDrawable(Color.parseColor(model.color)))
                .into(holder.binding.image)

            holder.binding.image.setOnClickListener{
                if(onClickListener != null){
                    onClickListener!!.onClick(position,model)
                }
            }

            holder.binding.root.setOnClickListener{
                if(onClickListener != null){
                    onClickListener!!.onClick(position,model)
                }
            }
        }
    }


    override fun getItemCount(): Int {
        return list.size
    }

    @SuppressLint("NotifyDataSetChanged")
    fun updateList(photos: List<Image>){
        list = photos as ArrayList<Image>
        notifyDataSetChanged()
    }

    interface OnClickListener{
        fun onClick(position: Int, model: Image)
    }

    private class MyViewHolder(binding: ImageItemBinding) : RecyclerView.ViewHolder(binding.root){
        var binding = binding
    }
}