package eu.tutorials.mobilechallenge.adapters

import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import eu.tutorials.mobilechallenge.R
import eu.tutorials.mobilechallenge.databinding.StoryItemBinding
import eu.tutorials.mobilechallenge.models.Image

open class StoryAdapter(
    private val context: Context,
    private var list: ArrayList<Image>
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var onClickListener : OnClickListener? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

        return MyViewHolder(
            StoryItemBinding.inflate(
                LayoutInflater.from(context),parent,
                false)
        )
    }

    fun setOnClickListener(onClickListener: OnClickListener){
        this.onClickListener = onClickListener
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val model = list[position]

        if (holder is MyViewHolder) {
            holder.binding.username.text = model.user.username

//            holder.binding.StoryView.setBackgroundResource(
//                R.drawable.custom_bg
//            )

            Glide.with(context)
                .load(model.user.profile_image.large)
                .placeholder(ColorDrawable(Color.parseColor(model.color)))
                .into(holder.binding.imageStory)

            holder.itemView.setOnClickListener{
                if(onClickListener != null){
                    onClickListener!!.onClick(position,model)
                }
            }
        }
    }


    override fun getItemCount(): Int {
        return list.size
    }



    interface OnClickListener{
        fun onClick(position: Int, model: Image)
    }

    private class MyViewHolder(binding: StoryItemBinding) : RecyclerView.ViewHolder(binding.root){
        var binding = binding
    }
}