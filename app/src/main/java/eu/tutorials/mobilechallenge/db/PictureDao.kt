package com.example.cleanunsplash.db


import androidx.room.*
import eu.tutorials.mobilechallenge.models.Image

@Dao
interface PictureDao {

    @Query("SELECT * FROM pictures")
    fun getAllPictures() : List<Image>

    @Insert
    fun insertPictures(pictures: List<Image>) : List<Long>
}