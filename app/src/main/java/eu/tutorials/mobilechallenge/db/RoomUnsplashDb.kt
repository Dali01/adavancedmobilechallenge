package com.example.cleanunsplash.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import eu.tutorials.mobilechallenge.models.Image

@Database(entities = [Image::class],version = 10, exportSchema = false)
abstract class RoomUnsplashDb:RoomDatabase(){
    abstract fun pictureDao():PictureDao


    companion object {
        @Volatile
        private var INSTANCE: RoomUnsplashDb? = null
        fun getInstance(context: Context): RoomUnsplashDb {
            synchronized(this) {
                var instance = INSTANCE
                if (instance == null) {
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        RoomUnsplashDb::class.java,
                        "unsplash_database"
                    )
                        .fallbackToDestructiveMigration()
                        .allowMainThreadQueries()
                        .build()
                    INSTANCE = instance
                }
                return instance
            }
        }
    }
}