package eu.tutorials.mobilechallenge.models

import androidx.room.*
import com.example.cleanunsplash.models.Urls
import com.example.cleanunsplash.models.User
import java.io.Serializable

@Entity(tableName = "pictures")
data class Image (
        @PrimaryKey(autoGenerate = true) @ColumnInfo(name = "uuid") val uuid : Int=0,
        @ColumnInfo(name = "id") val id: String?,
        @ColumnInfo(name = "width") val width: String?,
        @ColumnInfo(name = "height") val height: String?,
        @ColumnInfo(name = "color") val color: String?,
        @ColumnInfo(name = "created_at") val created_at: String?,
        @ColumnInfo(name = "updated_at") val updated_at: String?,
        @ColumnInfo(name = "description" , defaultValue = "No Description") val description: String?,

        @Embedded
        val urls: Urls,

        @Embedded
        val user: User,

        val likes: String?,

        ) : Serializable
