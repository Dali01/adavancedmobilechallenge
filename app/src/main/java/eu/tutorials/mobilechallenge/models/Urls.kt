package com.example.cleanunsplash.models

import java.io.Serializable

data class Urls (
    val raw : String?,
    val full : String?,
    val regular : String?,
    val thumb : String?
    ) : Serializable
