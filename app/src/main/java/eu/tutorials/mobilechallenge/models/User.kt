package com.example.cleanunsplash.models

import androidx.room.*
import java.io.Serializable

data class User (
    val idUser : String?,
    val username : String?,
    val name : String?,

    @Embedded
    val profile_image : ProfileImage,

    val TotalPhotos : String?,
    val collection : String?
        ) : Serializable