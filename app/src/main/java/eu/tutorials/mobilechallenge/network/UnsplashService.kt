package com.example.cleanunsplash.network


import eu.tutorials.mobilechallenge.Constants
import eu.tutorials.mobilechallenge.models.Image
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface UnsplashService {
    @GET("photos/?client_id=${Constants.APP_ID}")
    fun getRecentPhotos(
        @Query("page") page: Int,
        @Query("per_page") pageLimit: Int,
        @Query("order_by") order: String
    ) : Call<List<Image>>
}