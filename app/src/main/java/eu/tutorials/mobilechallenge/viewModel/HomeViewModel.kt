package eu.tutorials.mobilechallenge.viewModel

import android.app.Application
import android.content.Context
import android.content.SharedPreferences
import android.provider.SyncStateContract
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.example.cleanunsplash.db.RoomUnsplashDb
import com.example.cleanunsplash.network.UnsplashService
import com.google.gson.Gson
import eu.tutorials.mobilechallenge.Constants
import eu.tutorials.mobilechallenge.Constants.BASE_URL
import eu.tutorials.mobilechallenge.Constants.isNetWorkAvailable
import eu.tutorials.mobilechallenge.models.Image
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class HomeViewModel(app : Application) : AndroidViewModel(app) {
    var photoList : MutableLiveData<List<Image>> = MutableLiveData()

    private val pictureDao = RoomUnsplashDb.getInstance(getApplication()).pictureDao()


    init{
        if(Constants.isNetWorkAvailable(app)){
            getPicturesDetails()
        }else{
            getAllPictures()
        }
    }

    //Get Pictures from UNSPLASH API
    private fun getPicturesDetails(){

        val retrofit : Retrofit = Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        Log.e("Internet","Connection")

        val service: UnsplashService =
            retrofit.create<UnsplashService>(UnsplashService::class.java)

        val listCall : Call<List<Image>> = service.getRecentPhotos(
            1 , 30,"sort")


        listCall.enqueue(object : Callback<List<Image>> {
            override fun onResponse(
                call: Call<List<Image>>,
                response: Response<List<Image>>
            ) {
                if(response.isSuccessful){
                    photoList.postValue(response.body())
                    response.body()?.let { insertPictures(it) }
                    Log.i("Response Result","$photoList")
                }else{
                    val rc = response.code()
                    when(rc){
                        400 -> {
                            Log.e("Error 400" , "Bad Connection")
                        }
                        404 ->{
                            Log.e("Error 404" , "Not Found")
                        }
                        else ->{
                            Log.e("Error" , "$response")
                        }
                    }
                }
            }

            override fun onFailure(call: Call<List<Image>>, t: Throwable) {
                Log.e("Error's",t!!.message.toString())
            }


        })
    }


    //Get Pictures from our Offline Database
    private fun getAllPictures() {
        val list = pictureDao.getAllPictures()
        photoList.postValue(list)
        Log.e("ROOM","$list")
    }


    //Insert Pictures to our Offline database (ROOM) every time we consume UNSPLASH API
    private fun insertPictures(pictures: List<Image>){
        val ids = pictureDao.insertPictures(pictures)
        Log.e("id","$ids")
    }



    //Filter List
    fun filterList(filterItem: String) : MutableList<Image> {
        var templeList : MutableList<Image> = ArrayList()
        for(d in photoList.value!!){
            if(filterItem in d.user.username.toString()){
                templeList.add(d)
            }
        }

        return templeList
    }

}